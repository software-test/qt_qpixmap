#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "myvideocapture.h"

MainWindow::MainWindow(QWidget *parent)
    :QMainWindow(parent),
     ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mOpenCV_videoCapture = new MyVideoCapture(this);

    connect(mOpenCV_videoCapture , &MyVideoCapture::newPixmapCaptured, this , [&]()
    {
       ui->opencvframe->setPixmap(mOpenCV_videoCapture->pixmap().scaled(800,800));
    });
}

MainWindow::~MainWindow()
{
    delete ui;
    mOpenCV_videoCapture->terminate();
}


void MainWindow::on_Camera_clicked()
{
    mOpenCV_videoCapture->start(QThread::HighPriority);
}